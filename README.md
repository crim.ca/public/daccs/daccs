# DACCS

## Description

This project serves as the portal for the public Docker images used in the DACCS platform.

## Licenses

Licence information of the applications and/or libraries contained in the containers of this registry

- **daccs-eo-sentinelsat** : GNU General Public License v3.0
- **daccs-eo-snap8** : GNU General Public License v3.0


